package com.vchillara.SwaggerTest.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vchillara.SwaggerTest.model.Contact;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api")
public class AddressController {
	
	private ConcurrentHashMap<String, Contact> contacts = new ConcurrentHashMap<>();
	
	@GetMapping("/{id}")
	@ApiOperation(value="Get Contact By Id" )
	public Contact getContact(@ApiParam(value="Id of the contact") @PathVariable String id) {
		return contacts.get(id);
	}
	@PostMapping("/")
	@ApiOperation(value="Add a new contact to addressbook" )
	public Contact addContact(@RequestBody Contact contact) { 
		contacts.put(contact.getId(), contact);
		return contact;
	}
	
	
	@ApiOperation(value="Get All Contacts from addressbook" )
	@ApiResponses(value = { 

            @ApiResponse(code = 200, message = "Success|OK"),

            @ApiResponse(code = 401, message = "not authorized!"), 

            @ApiResponse(code = 403, message = "forbidden!!!"),

            @ApiResponse(code = 404, message = "not found!!!") })
	@GetMapping("/")
	public List<Contact> getAllContacts() {
		return new ArrayList<Contact>(contacts.values());
	}
}
