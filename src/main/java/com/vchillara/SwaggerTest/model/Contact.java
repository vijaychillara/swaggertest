package com.vchillara.SwaggerTest.model;

import io.swagger.annotations.ApiModelProperty;

public class Contact {

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhonenumber() {
		return phonenumber;
	}
	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}
	private String id;
	
	@ApiModelProperty(notes = "Name of the Contact",name="name",required=true,value="test name")
	private String name;
	private String phonenumber;
	
	
}
